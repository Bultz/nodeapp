// grunt.registerTask(taskName, [description, ] taskList)

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    jshint: {
      all: ['Gruntfile.js', '**/**/*.js', '**/*.js', '/*.js'],
      options: {
        reporter: require('jshint-json'),
        reporterOutput: 'tmp/report.json',
        force: true
      },
    },
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');



  grunt.registerTask('push', ['jshint']);;

};